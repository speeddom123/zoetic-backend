const http = require('http');
const app = require('./app')
const port = process.env.port || 8080;
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","*");
    next();
  });
const server = http.createServer(app);

server.listen(port);