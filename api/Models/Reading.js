const mangoose = require('mongoose');
const ReadingSchema = mangoose.Schema({
    _id: mangoose.Schema.Types.ObjectId,
    temperature: { type:Number },
    blood: { type:Number },
    bloodDenominator: { type:Number },
    oximeter: { type:Number },
    oximeterDenominator: { type:Number },
    date: { type: Date, required: true}
    
});

module.exports = mangoose.model('Reading',ReadingSchema);