const express = require("express");
const router = express.Router();
const Reading = require("../Models/Reading");
const mangoose = require("mongoose");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const Multer = require("multer");
const multer = require("multer");

const moment = require("moment");

// Routes
/**
 * @swagger
 * /reading/:
 *  get:
 *    tags:
 *      - ID params
 *    description: Use to request all Readings
 *    responses:
 *      '200':
 *        Array: [
    {
        "_id": "5f5546e5e2599e35645990dd",
        "temperature": 19,
        "blood": 43,
        "oximeter": 14,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
    {
        "_id": "5f5549b60b5dc73ab88cf021",
        "temperature": 55,
        "blood": 31,
        "oximeter": 26,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
]
 */

router.get("/", (req, res, next) => {
  Reading.find()
    .exec()
    .then((doc) => {
      console.log(doc);
      res.status(200).json(doc);
    })
    .catch((err) => {
      console.log(err);
      res.status(5000).json({ error: err });
    });
});
// Routes
/**
 * @swagger
 * /reading:
 *  post:
 *    tags:
 *      - ID params
 *    description: Use to Create New Reading. Reading Date is created at instance of Post Request
 *    parameters:
 *      - name: REading
 *        in: body
 *        description: The Reading to Create
 *        required: false
 *        schema:
 *          type: object
 *          properties:
 *               temperature:
 *                   type: integer
 *               blood:
 *                   type: integer
 *               bloodDenominator:
 *                   type: integer
 *               oximeter:
 *                   type: integer
 *               oximeterDenominator:
 *                  type: number
 *    responses:
 *      '201':
 *        description: Successfully created user
 */
router.post("/", (req, res, next) => {
  dateReq = new Date();
  let serviceDate = moment(dateReq);
  serviceDate = serviceDate.format("YYYY-MM-DD");
  console.log(req.body);
  const reading = new Reading({
    _id: new mangoose.Types.ObjectId(),

    temperature: req.body.temperature,
    blood: req.body.blood,
    bloodDenominator: req.body.bloodDenominator,
    oximeter: req.body.oximeter,
    oximeterDenominator: req.body.oximeterDenominator,
    date: serviceDate,
  });
  reading
    .save()
    .then((result) => {
      console.log(result);
      res.status(201).send("Successfully updated customer");;
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json(err);
    });
});

// Routes
/**
 * @swagger
 * /reading/{id}:
 *  get:
 *      tags:
 *          - ID params
 *      description: Get Reading by ID
 *      parameters:
 *          - name: id
 *            description: id to get by
 *            in: path
 *            type: id
 *      responses:
 *          '200':
 *            Array: [
    {
        "_id": "5f5546e5e2599e35645990dd",
        "temperature": 19,
        "blood": 43,
        "oximeter": 14,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
    {
        "_id": "5f5549b60b5dc73ab88cf021",
        "temperature": 55,
        "blood": 31,
        "oximeter": 26,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
]
 */

router.get("/:id", (req, res, next) => {
  const eventID = req.params.id;
  Reading.findById({ _id: eventID })
    .exec()
    .then((doc) => {
      console.log(doc);
      res.status(200).json(doc);      
    })
    .catch((err) => {
      console.log(err);
      res.status(5000).json({ error: err });
    });
});
// Routes
/**
 * @swagger
 * /reading/date/{date}:
 *  get:
 *      tags:
 *          - Date params
 *      description: Get Reading by Date
 *      parameters:
 *          - name: date
 *            description: date to get by
 *            in: path
 *            type: date
 *      responses:
 *          '200':
 *            Array: [
    {
        "_id": "5f5546e5e2599e35645990dd",
        "temperature": 19,
        "blood": 43,
        "oximeter": 14,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
    {
        "_id": "5f5549b60b5dc73ab88cf021",
        "temperature": 55,
        "blood": 31,
        "oximeter": 26,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
]
 */
router.get("/date/:date", (req, res, next) => {
  const date = req.params.date;
  Reading.findOne({ date: date })
    .sort({ _id: -1 })
    .exec()
    .then((doc) => {
      if (doc === null) {
        console.log("No results found");
        res.status(500).json({ message: "No data for date" });
      } else {
        console.log(doc);
        res.status(200).json(doc);
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err, message: "No data for date" });
    });
});
// Routes
/**
 * @swagger
 * /reading/{id}:
 *  patch:
 *      tags:
 *          - ID params
 *      description: Patch  by id with req body
 *      parameters:
 *          - name: id
 *            description: id to get Updated
 *            in: path
 *            type: id
 *          - name: reqBody
 *            description: request Body
 *            in: body
 *            schema:
 *              type: object
 *              properties:
 *                  temperature:
 *                      type: integer
 *                  blood:
 *                      type: integer
 *                  oximeter:
 *                      type: integer
 *                  bloodDenominator:
 *                      type: integer
 *                  oximeterDenominator:
 *                      type: integer
 *                  date:
 *                      type: date
 *      responses:
 *          '200':
 *            Array: [
    {
        "_id": "5f5546e5e2599e35645990dd",
        "temperature": 19,
        "blood": 43,
        "oximeter": 14,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
    {
        "_id": "5f5549b60b5dc73ab88cf021",
        "temperature": 55,
        "blood": 31,
        "oximeter": 26,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
]
 */
router.patch("/:id", (req, res, next) => {
  const eventID = req.params.id;
  Reading.update(
    { _id: eventID },
    {
      $set: {
        temperature: req.body.temperature,
        blood: req.body.blood,
        bloodDenominator: req.body.bloodDenominator,
        oximeter: req.body.oximeter,
        oximeterDenominator: req.body.oximeterDenominator,
        date: req.body.date,
      },
    }
  )
    .exec()
    .then((doc) => {
      console.log(doc);
      res.status(200).json(doc);
    })
    .catch((err) => {
      console.log(err);
      res.status(5000).json({ error: err });
    });
});
// Routes
/**
 * @swagger
 * /reading/{id}:
 *  delete:
 *      tags:
 *          - ID params
 *      description: Delete  by id
 *      parameters:
 *          - name: id
 *            description: id to get delete
 *            in: path
 *            type: id
 *      responses:
 *          '200':
 *            Array: [
    {
        "_id": "5f5546e5e2599e35645990dd",
        "temperature": 19,
        "blood": 43,
        "oximeter": 14,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
    {
        "_id": "5f5549b60b5dc73ab88cf021",
        "temperature": 55,
        "blood": 31,
        "oximeter": 26,
        "date": "2020-09-06T00:00:00.000Z",
        "__v": 0
    },
]
 */
router.delete("/:id", (req, res, next) => {
  Reading.remove({ _id: req.params.id })
    .exec()
    .then((result) => {
      console.log(result);

      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500), json9(err);
    });
});

module.exports = router;
