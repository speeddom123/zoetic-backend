const express = require('express');
const mangoose = require('mongoose');
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const app = express();
const morgan = require('morgan');
const ReadingRoutes = require('./api/routes/Readings');

const bodyparser =require('body-parser');

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        version: "1.0.0",
        title: "ZoeTec Api",
        description: "Api to save Random Reading from Zoetec App",
        contact: {
          name: 'Dominique Griffith'
        },
        servers: ["http://localhost:8080"]
      }
    },
    // ['.routes/*.js']
    apis:  ['./api/routes/*.js']
  };
  
  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
mangoose.connect('mongodb+srv://eventband:'+ process.env.MONGO_ATLAS_PW+'@eventband-jwmms.mongodb.net/<dbname>?retryWrites=true&w=majority',
{
    useNewUrlParser :true,
    useUnifiedTopology: true
})
app.use(morgan('dev'));

app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())


app.use('/reading', ReadingRoutes);

//error
app.use((req,res,next) =>{
    const error = new Error('not Found');
    error.status=404;
    next(error);
});

app.use((error,req,res,next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
                error:{
                    message: error.message
                }
        }
    })
});

module.exports = app;